#!/bin/bash

WORKINGDIR=$(dirname $0)
procs=$(nproc)
 
main(){
    echoGreen "Running on ${procs} CPUs."

    git submodule update --init --recursive --force

    pushd ${WORKINGDIR}/godot
    linkModule "../godot_voxel" voxel

    buildPlatform $1
    
    rm $moduleLinks
    popd

    #publish:
    mkdir -p bin
    mv ${WORKINGDIR}/godot/bin/* bin
}

moduleLinks=""
linkModule(){
    module=$1
    target="modules/${2}"

    if [[ -L "${target}" ]]
    then
        echo "${target} is here!"
    else
        ln -s "${module}" "${target}"
    fi

    moduleLinks="$moduleLinks \"${target}\""

}
clearModules(){
    rm $moduleLinks
    moduleLinks=""
}

buildPlatform(){
    platform="$1"
    echoGreen "Compiling for ${platform}"
    scons -j${procs} platform="${platform}" bits=64 tools=yes target=release_debug --cache-populate
    scons -j${procs} platform="${platform}" bits=64 tools=no target=release_debug --cache-populate
    scons -j${procs} platform="${platform}" bits=64 tools=no target=release --cache-populate
}

echoGreen(){
    echo -e "\e[32m$@\e[39m"
}

main $@
