extends gSDF

class_name gSD_Sphere

export(float) var radius = 0;

func _init(radius):
	self.radius = radius

func resolve(p:Vector3)->float:
	return p.length()-radius;
