extends gSDF

class_name gSD_Box

export(Vector3) var size = Vector3.ONE;

func _init(size):
	self.size = size

func resolve(p:Vector3)->float:
	var q = p.abs() - size
	var a = Vector3(max(q.x,0.0) , max(q.y,0) ,max(q.z,0)).length() 
	var b = min(max(q.x,max(q.y,q.z)),0.0)
	return a + b;


#float sdBox( vec3 p, vec3 b )
#{
#  vec3 q = abs(p) - b;
#  return length(max(q,0.0)) + min(max(q.x,max(q.y,q.z)),0.0);
#}
