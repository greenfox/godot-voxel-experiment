extends Resource

class_name gSDF

var sdf = load("res://sdf/gd_sdf.gd")

enum OP{
	RES,#resolve
	ADD,
	SUBTRACT,#subtract
	INTERSECT,#intersect
	TRANSLATE,#transformation
	ROTATE,#ROTATE
	TRANSFORM,#TRANSFORM
}

export(OP) var operator = OP.RES;

var a:gSDF;
var b;
var c;

func operation(_a,_OP,_b=null,_c=null):#these should probably be arrays?
	a = _a
	operator = _OP
	b = _b
	c = _c
	return self

func colapse():
	#todo, seek through members and colapse values as able, for example:
	#easy: sequential transforms can be combined
	#subtractions/additions can obsolete objects that exist within their bounds
	return self
	
func resolve(p:Vector3)->float:
	match operator:
		OP.RES:
			push_error("resolve should be overloaded!")
		OP.ADD:
			return min(a.resolve(p),b.resolve(p));
		OP.SUBTRACT:
			return max(a.resolve(p),-b.resolve(p));
		OP.INTERSECT:
			return max(a.resolve(p),b.resolve(p));
		OP.TRANSLATE:
			return a.resolve(p - b)
		OP.ROTATE:
			return a.resolve(p.rotated(b,c))
		OP.TRANSFORM:
			return a.resolve(b*p);

	push_error("resolve not handled!")
	return INF;

func add(input:gSDF)->gSDF:
	return sdf.new().operation(self,OP.ADD,input)
	
func subtract(input:gSDF)->gSDF:
	return sdf.new().operation(self,OP.SUBTRACT,input)
	
func intersect(input:gSDF)->gSDF:
	return sdf.new().operation(self,OP.INTERSECT,input)

func translate(input:Vector3)->gSDF:
	return sdf.new().operation(self,OP.TRANSLATE,input)

func rotate(axis,angle)->gSDF:
	return sdf.new().operation(self,OP.ROTATE,axis,angle)
