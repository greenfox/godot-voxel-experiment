shader_type canvas_item;

uniform vec3 origin = vec3(0,0,0);
uniform float stride = 1;
uniform float radius = 16;

const float CELLS = 16.;


vec3 point(vec2 input){
	return vec3(floor(input.x * CELLS)/CELLS,input.y,mod(input.x*CELLS,1.));
}
vec3 space(vec3 point){
	return origin + (point*stride*CELLS);
}

const vec4 WHITE = vec4(1,1,1,1);
const vec4 BLACK = vec4(0,0,0,1);

void fragment() {
	vec3 a = space(point(UV));
	if(length(a) < radius){
		COLOR = WHITE;
	}
	else{
		COLOR = BLACK;
	}
}





