tool
extends VoxelGenerator

class_name SDF_VoxelGen

var sdfCalc:gSDF;

var channel:int = VoxelBuffer.CHANNEL_SDF

export(float) var radius = 100;

func get_used_channels_mask () -> int:
	return 1<<channel

func ready():
	var a = gSD_Sphere.new(radius)
	var b = gSD_Sphere.new(radius*0.5).translate(Vector3(0,radius,0))
	var c = gSD_Box.new(Vector3(radius,radius*0.25,radius*0.25) ).translate(Vector3(radius,0,0))
	sdfCalc =  a.subtract(b).subtract(c);
	var f = sdfCalc.resolve(Vector3.ZERO)
	pass

func generate_block(out_buffer:VoxelBuffer, origin:Vector3, lod:int) -> void:
	var stride:int = 1 << lod
	var size:Vector3 = out_buffer.get_size()

	var gz:int = origin.z
	for z in range(0, size.z):

		var gx:int = origin.x
		for x in range(0, size.x):

			var gy:int = origin.y
			for y in range(0, size.y):
				var out = sdfCalc.resolve(Vector3(gx,gy,gz))/stride
				out_buffer.set_voxel_f(out,x,y,z,channel)
				gy += stride
			gx += stride
		gz += stride
