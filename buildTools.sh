#!/bin/bash

procs=$(nproc)

godotDir="$(dirname $0)/engine/godot"

main(){
    echoGreen "Running on ${procs} CPUs."

    git submodule update --init --recursive --force
    pushd ${godotDir}
    linkModule "../../godot_voxel" voxel

    buildSingle $1
    
    clearModuleLinks
    popd
    mkdir -p bin
    mv ${godotDir}/bin/* bin
}

linkModule(){
    module=$1
    target=$2
    if [[ -L "modules/${target}" ]]
    then
        echo "${target} is here!"
    else
        ln -s "${module}" "modules/${target}"
    fi
}
clearModuleLinks(){
    rm $( find ${godotDir}/modules/voxel -type l )
}

buildSingle(){
    scons -j${procs} bits=64 tools=yes target=release_debug --cache-populate $@
}

echoGreen(){
    echo -e "\e[32m$@\e[39m"
}

main $@
